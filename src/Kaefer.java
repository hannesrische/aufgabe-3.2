import java.util.*;

/**
 * 
 */
public class Kaefer {
	
	/**
	 * wenn es von einer flaeche mehrere Wege gibt weiter zu gehen, dann wird
	 * diese flaeche auf den stack gelegt
	 */
	private Stack<Integer> entscheidungen = new Stack<Integer>();
	
	/**
	 * hier werden die bisher betretenen flaechen gespeichert
	 */
	private int[] aktuellerPfad = new int[19];
	
	/**
	 * hier wird der bisher beste pfad gespeichert
	 */
	private int[] minimalerPfad = new int[19];
	
	/**
	 * dauer des aktuell geprueften pfades zum aktuell geprueften punkt
	 */
	private int aktuelleZeit = -1;
	
	/**
	 * dauer des bisher besten pfades
	 */
	private int minimaleZeit = -1;
	
	/**
	 * ausgangsflaeche
	 */
	private int startfeld = -1;
	
	/**
	 * aktuell waehlbare kanten
	 */
	private ArrayList<int[]> aktuelleKantenmenge = new ArrayList<int[]>();
	
	/**
	 * von der ausgangsmenge im aktuellen lauf entfernte kanten
	 */
	private Stack<int[]> entfernteKanten = new Stack<int[]>();
	
	/**
	 * ikosader fuer den die berechnungen durchgefuehrt werden
	 */
	private Ikosaeder ikosaeder; 
	
	/**
	 * Konstruktor. zufaelliges startfeld
	 */
	public Kaefer(Ikosaeder ikosaeder) {
		this.ikosaeder = ikosaeder;
		this.startfeld = (int)(Math.random()*19)+1;
	}
	
	/**
	 * Konstruktor. fest gewaehltes startfeld
	 *
	 */
	public Kaefer(Ikosaeder ikosaeder, int startfeld) {
		this.ikosaeder = ikosaeder;
		this.startfeld = startfeld;
	}
	
	/**
	 * 
	 */
	public void setAktuelleKantenmenge () {
		this.aktuelleKantenmenge = this.ikosaeder.getKantenmenge();
	}
	
	/**
	 * 
	 */
	public ArrayList<int[]> getAktuelleKantenmenge () {
		return this.aktuelleKantenmenge;
	}
	
	/**
	 * hier ist der eigentliche algorithmus
	 */
	public void findeSchnellstenPfad () {
		
//		Feld um Menge aller Nachfolger zu bestimmen
		ArrayList<int[]> auswahl = new ArrayList<int[]>();

//		Bestimme einen Pfad
		for (int i = 0; i < this.aktuellerPfad.length; i++) {
			//erstes Feld ist Startfeld
			if (i == 0) {
				aktuellerPfad[0] = startfeld;
			} else {
//				Finde alle Nachfolger in der Kantenmenge
				for (int j = 1; j < this.aktuelleKantenmenge.size(); j++) {
					if (this.aktuelleKantenmenge.get(j)[0] == aktuellerPfad[j-1] || this.aktuelleKantenmenge.get(j)[1] == aktuellerPfad[j-1]) {
						auswahl.add(this.aktuelleKantenmenge.get(j));
					}
				}
//				auswahl sollte jetzt 0, 1, 2 oder 3 Elemente haben

//				Wenn auswahl leer ist folgt:
//				
//				entweder Pfadende: wenn minimal, dann update minimalerPfad, minimaleZeit, 
//				gehe zu letzem Auswahlpunkt zurück
//				
//				oder keine weitere Auswahlmoeglichkeit: gehe zu letztem Auswahlpunkt zurueck
				if (auswahl.isEmpty()) {
					
				}
				
//				waehle einen der Nachfolger
				int[] ausgewaehlt = auswahl.get((int)Math.random()*(auswahl.size()-1));
				
//				schreibe Nachfolger in den aktuellen Pfad
				if (ausgewaehlt[0] == aktuellerPfad[i-1]) {
					aktuellerPfad[i] = ausgewaehlt[1];
				} else {
					aktuellerPfad[i] = ausgewaehlt[0];
				}

//				entferne die Kante zum gewaehlten Nachfolger aus aktuelleKantenmenge
//				lege diese Kant auf den Stack entfenteKanten
				aktuelleKantenmenge.remove(ausgewaehlt);
				entfernteKanten.push(ausgewaehlt);

//				wenn mehr als ein Nachfolger zur Auswahl stand, dann schiebe den Knoten
//				auf den entscheidungen Stack
				if (auswahl.size() > 1) {
					entscheidungen.push(aktuellerPfad[i]);
				}
				
//				leere die Menge der Nachfolger
				auswahl.clear();
				
//				markiere die flaeche als betreten
				ikosaeder.setBetreten(aktuellerPfad[i]);
					
			}
		}
	}
	
	/**
	 * formatierte Ausgabe
	 */
	public void ergebnisAusgabe () {
		System.out.println("kürzester Weg");
		for (int i = 0; i < 20; i++) {
			System.out.print(this.minimalerPfad[i]);
			if (i < 19) {
				System.out.print(", ");
			}
			else {
				System.out.print("\n");
			}
		}
		System.out.println("Dauer: "+minimaleZeit+" s");
	}
	
	public static void main (String[] args) {
		
	}
	
}
