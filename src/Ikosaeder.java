import java.util.*;

/**
 * Die Klasse erstellt einen Ikosaeder.
 * 
 *
 */
public class Ikosaeder {

	/**
	 * Spalte 1: default Flaechennamen
	 * Spalte 2: neue Flachennamen -> verwende eingabe(int[20] werte), das ueberschreibt
	 * die 2 Spalte mit den neu zugewiesenen Namen 
	 */
	private int[][] flaechenwerte = new int[20][2];
	
	/**
	 * ArrayListe von Paaren existierender ungerichteter Kanten
	 * Form (v1,v2) mit v1 < v2
	 */
	private ArrayList<int[]> kantenmenge = new ArrayList<int[]>();
	
	/**
	 * Feld mit 20 Positionen zum markieren welche flächen schon betreten wurden
	 * verwende setBetreten(int index)
	 */
	private boolean[] betreten = new boolean[20];
	
	/**
	 * Konstruktor.
	 * Initialisiert die default Flaechenwerte mit 1,...,20
	 * Initialisiert eine gueltige Kantenmenge
	 * Initialisiert betreten mit false
	 */
	public Ikosaeder() {
		
		for (int i = 0; i < 20; i++) {
			this.flaechenwerte[i][0] = i+1;
		}
		
		kantenmenge.add(new int[] {1,2});
		kantenmenge.add(new int[] {2,3});
		kantenmenge.add(new int[] {3,4});
		kantenmenge.add(new int[] {4,5});
		kantenmenge.add(new int[] {1,5});
		kantenmenge.add(new int[] {6,7});
		kantenmenge.add(new int[] {7,8});
		kantenmenge.add(new int[] {8,9});
		kantenmenge.add(new int[] {9,10});
		kantenmenge.add(new int[] {10,11});
		kantenmenge.add(new int[] {11,12});
		kantenmenge.add(new int[] {12,13});
		kantenmenge.add(new int[] {13,14});
		kantenmenge.add(new int[] {14,15});
		kantenmenge.add(new int[] {6,15});
		kantenmenge.add(new int[] {17,18});
		kantenmenge.add(new int[] {18,19});
		kantenmenge.add(new int[] {19,20});
		kantenmenge.add(new int[] {16,20});
		kantenmenge.add(new int[] {16,17});
		kantenmenge.add(new int[] {1,6});
		kantenmenge.add(new int[] {2,8});
		kantenmenge.add(new int[] {3,10});
		kantenmenge.add(new int[] {4,12});
		kantenmenge.add(new int[] {5,14});
		kantenmenge.add(new int[] {15,16});
		kantenmenge.add(new int[] {7,17});
		kantenmenge.add(new int[] {9,18});
		kantenmenge.add(new int[] {11,19});
		kantenmenge.add(new int[] {13,20});
		
		for (int i = 0; i < betreten.length; i++) {
			betreten[i] = false;
		}
		
	}
	
	/**
	 * ueberschreibt spalte 2 der flaechenwertematrix mit dem uebergebenen array
	 * @param werte
	 */
	public void eingabe(int[] werte) {
		if (werte.length != 20) {
			System.out.println("Länge des Eingabe-Arrays ist nicht 20");
		} else {
			for (int i = 0; i < 20; i++) {
				this.flaechenwerte[i][1] = werte[i];
	//			System.out.println("["+flaechenwerte[i][0]+","+flaechenwerte[i][1]+"]");
			}
			kantenmengeAktualisieren();
		}
	}
	
	/**
	 * ersetzt die flaechenwerte in der Kantenmenge entsprechend der zuweiseung
	 * die sich aus der matrix flaechenwerte ergibt
	 */
	private void kantenmengeAktualisieren() {
		for (int i = 0; i < kantenmenge.size(); i++) {
				kantenmenge.get(i)[0] = this.flaechenwerte[kantenmenge.get(i)[0]-1][1];
				kantenmenge.get(i)[1] = this.flaechenwerte[kantenmenge.get(i)[1]-1][1];
		}
	}
	
	public ArrayList<int[]> getKantenmenge() {
		return this.kantenmenge;
	}
	
	public void setBetreten(int index, boolean value) {
		betreten[index] = value;
	}
	
	public boolean getBetreten(int index) {
		return this.betreten[index];
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ikosaeder test = new Ikosaeder();
		
		ArrayList<int[]> kantenmenge = test.getKantenmenge();
//		System.out.println("alte Liste");
//		for (int i = 0; i < kantenmenge.size(); i++) {
//			System.out.println("["+kantenmenge.get(i)[0]+","+kantenmenge.get(i)[1]+"]");
//		}
		
		int[] werte = {20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
		test.eingabe(werte);
		
//		System.out.println("neue Liste");
//		for (int i = 0; i < kantenmenge.size(); i++) {
//			System.out.println("["+kantenmenge.get(i)[0]+","+kantenmenge.get(i)[1]+"]");
//		}
	}

}
	